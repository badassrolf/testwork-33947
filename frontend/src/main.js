import Vue from 'vue'
import App from './App.vue'
import axios from "axios";
import {API_URL} from "./constants/api";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/ru-RU'
import Vuex from 'vuex'
import store from './store'

axios.defaults.baseURL = API_URL;
axios.defaults.headers.get['Accept'] = 'application/json';
axios.defaults.headers.post['Accept'] = 'application/json';
axios.interceptors.response.use(function (response) {
  return response.data.data
}, function (error) {
  // Do something with response error
  let response = '';

  if (error.response.status === 422) {
    for (const value of Object.entries(error.response.data.errors)) {
      response += ' ' + value[1] + ', ';
    }
  }
  return Promise.reject(response);
});

Vue.config.productionTip = false
Vue.use(ElementUI, {locale});
Vue.use(Vuex)

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
