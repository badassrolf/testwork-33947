import axios from 'axios';

export default {
  state: {
    users: [],
  },
  getters: {
    users:  state => state.users,
  },
  mutations: {
    setUsers (state, commit) {
      state.users = commit
    },
  },
  actions: {
      getUsers({commit}) {
          return axios.get('user').then(response => {
              commit('setUsers', response);
          });
      },

      deleteUser({commit}, payload) {
          return axios.delete('user/'+payload.id);
      },

      createUser({commit}, payload) {
          return axios.post('user', {
              name: payload.name,
              email: payload.email,
              phone: payload.phone,
              password: payload.password,
          })
      }
  }
}
