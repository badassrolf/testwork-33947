<?php

namespace App\Services;

use App\Entity\CreateUserDTO;
use App\Entity\UpdateUserDTO;
use App\Exceptions\UserNotFoundException;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

/**
 * Сервис для работы с пользователями
 *
 * Class UserService
 * @package App\Services
 */
class UserService
{
    /**
     * Создание пользователя
     *
     * @param CreateUserDTO $createUserDTO
     * @return User
     */
    public function createUser(CreateUserDTO $createUserDTO): User
    {
        $user = new User();
        $user->setEmail($createUserDTO->getEmail());
        $user->setName($createUserDTO->getName());
        $user->setPhone($createUserDTO->getPhone());
        $user->setPassword(Hash::make($createUserDTO->getPassword()));
        $user->save();

        return $user;
    }

    /**
     * Обновление пользователя
     *
     * @param int $userId
     * @param UpdateUserDTO $updateUserDTO
     * @return User
     * @throws UserNotFoundException
     */
    public function updateUser(int $userId, UpdateUserDTO $updateUserDTO): User
    {
        $user = User::find($userId);

        if (empty($user)) {
            throw new UserNotFoundException('Пользователь не найден');
        }

        $user->setEmail($updateUserDTO->getEmail());
        $user->setName($updateUserDTO->getName());
        $user->setPhone($updateUserDTO->getPhone());
        $user->save();

        return $user;
    }
}
