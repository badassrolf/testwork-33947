<?php

namespace App\Http\Controllers;

use App\Exceptions\UserNotFoundException;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Контроллер для работы с пользователем
 *
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    public function __construct(private UserService $userService)
    {
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return UserResource::collection(User::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUserRequest $request
     * @return UserResource
     */
    public function store(CreateUserRequest $request)
    {
        return new UserResource($this->userService->createUser($request->getCreateUserDTO()));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return UserResource
     */
    public function show($id)
    {
        $user = User::find($id);

        if (empty($user)) {
            throw new UserNotFoundException('Пользователь не найден');
        }

        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param $id
     * @return UserResource
     */
    public function update(UpdateUserRequest $request, $id)
    {
        return new UserResource($this->userService->updateUser($id, $request->getUpdateUserDTO()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return JsonResource
     * @throws UserNotFoundException
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (empty($user)) {
            throw new UserNotFoundException('Пользователь не найден');
        }

        return JsonResource::make([
            'result' => $user->delete()
        ]);
    }
}
