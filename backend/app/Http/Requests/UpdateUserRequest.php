<?php

namespace App\Http\Requests;

use App\Entity\UpdateUserDTO;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Запрос обновление пользователя
 *
 * Class UpdateUserRequest
 * @package App\Http\Requests
 */
class UpdateUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'phone' => 'required',
            'name' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Email обязателен',
            'email.email' => 'Email имеет неверный формат ',
            'phone.required' => 'Телефон обязателен',
            'name.required' => 'Имя обязательно',
        ];
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getUpdateUserDTO()
    {
        return new UpdateUserDTO($this->getEmail(), $this->getName(), $this->getPhone());
    }
}
