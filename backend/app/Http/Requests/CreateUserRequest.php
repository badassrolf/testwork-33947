<?php

namespace App\Http\Requests;

use App\Entity\CreateUserDTO;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Запрос создания пользователя
 *
 * Class CreateUserRequest
 * @package App\Http\Requests
 */
class CreateUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'phone' => 'required',
            'name' => 'required',
            'password' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Email обязателен',
            'email.email' => 'Email имеет неверный формат ',
            'phone.required' => 'Телефон обязателен',
            'name.required' => 'Имя обязательно',
            'password.required' => 'Пароль обязателен',
        ];
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function getCreateUserDTO()
    {
        return new CreateUserDTO($this->getEmail(), $this->getName(), $this->getPhone(), $this->getPassword());
    }
}
