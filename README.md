# Разворачивание
- cp backend/.env.example backend/.env  
- прописываем переменные для докера
````
  COMPOSE_DB_PORT
  COMPOSE_NGINX_PORT
````
- указываем базу данных. Хости и порт оставляем именно такой  
````
DB_CONNECTION=mysql
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
DB_ROOT_PASSWORD=
DB_HOST=db
DB_PORT=3306
````
- docker-compose up -d  
- заходим в контейнер backend_php_1
- накатываем миграции и сидеры, для отображения пользователей  
- настриваем конфиг nginx, чтобы работало на одном домене, или ставим на бэк https
